const express = require('express')
const app = express()
const port = 3000

app.use(express.static('src/public'))

app.get('/api', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Wish Whale listening at http://localhost:${port}`)
})
